from math import floor
import logging

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)
lg = logging.getLogger(__name__)

def nwd(a,b):
    if b == 0:
        return a
    else:
        return nwd(b,a % b)

def euclides(a,b):
    lg.debug("Euclides({},{})".format(a,b))
    if a < b:
        return euclides(b,a)
    else:
        divider = int(floor(a/b))
        remainder = a % b
        if remainder <= 0:
            return b
        else:
            res =  euclides(b, remainder)
            lg.debug('Euclides ({},{}) - {}'.format(a,b,res))
            if isinstance(res, int):
                return (1, -divider)
            else:
                # lewy wynik dla b , prawy dla remainder
                # a to nasz najwyższy
                (x,y) = res
                return (y, x  - divider * y)

def chtr(stmts):
    M = 1
    s = 0
    for (a,m) in stmts:
        M *= m

    lg.debug("M: {}".format(M))

    for (a,m) in stmts:
        ma = int(M / m)
        lg.debug("Euc for ({},{})".format(ma, m))
        (x,y) = euclides(ma, m)
        lg.debug("Res EUC: ({},{})".format(x,y))
        n = x
        lg.debug("MA: {}, N: {}".format(ma, n))
        s = s + a * ma * n

    lg.debug('Suma chtr {}'.format(x))
    s = s % M
    lg.debug('Wynik chtr: {}'.format(x))
    return s
